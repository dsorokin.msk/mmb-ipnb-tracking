## Introduction

The project contains the code for compensation for cell global motion and iPNB particles tracking and track analysis.
It is the supplementary code to the paper 

Sorokin D.V., Arifulin E.A., Vassetzky Y.S., Sheval E.V. Live-cell imaging and analysis of nuclear body mobility. Methods in Molecular Biology. 2020. (accepted).

The detailed instructions on using the software can be found in the paper.

## System requirements

The code runs on Windows 10 with Matlab 2018a.

## Installation

1. Install DipImage 2.9 (http://www.diplib.org/download).
2. Clone the repository to desired location.
3. Unpack the sofa-bin.zip.
4. Open ipnb_tracking\main_script.m 
5. Make sure the paths to SOFA binaries in the 'Nuclei motion compensation' section are correct.
6. Follow the step-by-step script to run the code on the example data.

## Dependencies

We acknowledge the use of the following software in this project:
* DipImage (http://www.diplib.org)
* SOFA (https://www.sofa-framework.org/)
* Mesh2d v.2.4 (https://github.com/dengwirda/mesh2d)
* export_fig toolbox (https://github.com/altmany/export_fig)

## References

In case of using the provided software please properly cite the following papers.

The complete description of the non-rigid cell image registration approach can be found in:

[1] Sorokin, D. V., Peterlik, I., Tektonidis, M., Rohr, K., & Matula, P. (2017). Non-Rigid Contour-Based Registration of Cell Nuclei in 2-D Live Cell Microscopy Images Using a Dynamic Elasticity Model. IEEE transactions on medical imaging, 37(1), 173-184.

The description of tracks analysis and motion type analysis can be found in:

[2] Arifulin, E. A., Sorokin, D. V., Tvorogova, A. V., Kurnaeva, M. A., Musinova, Y. R., Zhironkina, O. A., Golyshev, S. A., Abramchuk, S. S., Vassetzky, Y. S., & Sheval, E. V. (2018). Heterochromatin restricts the mobility of nuclear bodies. Chromosoma, 127(4), 529-537.

The description of spot detection can be found in:

[3] Foltánková, V., Matula, P., Sorokin, D., Kozubek, S., & Bártová, E. (2013). Hybrid detectors improved time-lapse confocal microscopy of PML and 53BP1 nuclear body colocalization in DNA lesions. Microscopy and Microanalysis, 19(2), 360-369.

## Contact information
dsorokin@cs.msu.ru