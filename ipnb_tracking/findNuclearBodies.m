function findNuclearBodies(fname, par, par_nucleoli, verbose, save_overlays)

if nargin < 3
    verbose = 0;
end

if nargin < 4
    save_overlays = 1;
end

cellMsuffix = '_body.tif';
cellm = readimTiff([fname(1:end-4) cellMsuffix])>0;

if (isempty(par.finishInd))
    par.finishInd = size(cellm,3)-1;
end

ics = readimTiff(fname);
if any(imsize(ics) ~= imsize(cellm))
    error('Size of cellm ~= size of ics')
end
    
%% measure some statistics
m = measure(slice_ex(cellm,0), slice_ex(cellm,0)*255, {'perimeter','DimensionsEllipsoid','Size'});
cellStat = struct;
% cellStat.contour_sampling_dist = m(1).Perimeter / par.pntsN;
cellStat.cell_ellipsoid_size = m(1).DimensionsEllipsoid;
cellStat.cell_area = m(1).Size;
fn_structdisp(cellStat)

par.cellStat = cellStat;

ics = ics(:,:,par.startInd:par.finishInd);
cellm = cellm(:,:,par.startInd:par.finishInd);
%% find nucleoli
[spotsNucl, smaskIcsNucl, ovlIcsNucl] = findSpotsIcsTr(ics, cellm, par_nucleoli);

if (verbose)
    dipshow(ovlIcsNucl)
    dipstep ON
end

%% find iPNBs

% par.recomputeSpots = 1;
% [spots, smaskIcs, ovlIcs] = findSpotsIcs(ics, cellm, par, fname);
% [spots, smaskIcs, ovlIcs] = readSpots(fname, spotsSuffix, smaskSuffix, ovlSuffix)

[spots, smaskIcs, ovlIcs] = findSpotsIcs(ics, cellm-smaskIcsNucl, par);

if (verbose)
    dipshow(ovlIcs)
    dipstep ON
end

spotsPart = spots;

%% save results
smaskIcs1 = smaskIcs + smaskIcsNucl;
spots1 = [];
for i=1:length(spotsNucl)
    spots1{i} = [spots{i}; spotsNucl{i}];
end
spots = spots1;

writeSpots(spots, fname, 'spots');
ICYspotsWrite(spots, [fname(1:end-4) '_spots_ICY.txt']);
writeimTiff(smaskIcs1, [fname(1:end-4) '_smaskIcs.tif']);

if (save_overlays)
    smaskIcsE = slice_op('berosion', smaskIcs1, 1);
    smaskIcsC = smaskIcs1-smaskIcsE;
    ovlIcs1 = overlay(ics, smaskIcsC, [255 0 0]);
    writeimTiff(ovlIcs1, [fname(1:end-4) '_ovlIcs.tif']);
end

end
