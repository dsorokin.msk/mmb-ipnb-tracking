function [tl, stat, statDir, nbInd, motionType] = analyzeTracksMotionType(fname, pout, stat_options)
% performs track analysis for motion type 
% 

if nargin < 3
    stat_options = struct;
end

if ~isfield(stat_options, 'xls_path')
    stat_options.xls_path = [];
end

if ~isfield(stat_options, 'px_size')
    stat_options.px_size = 0.04;
end

if ~isfield(stat_options, 'delta_t')
    stat_options.delta_t = 1.0;
end

if ~isfield(stat_options, 'save_msd')
    stat_options.save_msd = 0;
end

close all;

[fpath, fnameNE, ~] = fileparts(fname);
fpath = [fpath filesep];
statpath = [pout '-' 'stat'];
statpath = [filesep statpath filesep];
mkdir([fpath filesep statpath]);

ics = readimTiff([fpath fnameNE '.tif']);
writeim(slice_ex(ics,0), [fpath statpath fnameNE '_1frame.tif']);

im = readim([fpath statpath fnameNE '_1frame.tif']);

cellm = readimTiff([fpath fnameNE '_body.tif']);
m = measure(slice_ex(cellm,0), slice_ex(cellm,0)*255, 'Center');

seqLength = imsize(ics,3);

%%
tl = load([fpath pout filesep fnameNE '_tracks_reg.mat']);
tl = tl.tracks_reg;

%%
try 
    smask = readimTiff([fpath fnameNE '_smaskIcs.tif']);
    tlNew3 = addAreaInfo(smask, tl);
    save([fpath statpath fnameNE(1:end) '_tracks_reg_w_area.mat'], 'tlNew3');
    tl = tlNew3;
catch ME
    error(['Can not read the ' fpath fnameNE '_smaskIcs.tif' ' file and add spots area info!']) 
end

%% statistics

[stat] = computeTrackStatistics(tl, stat_options.delta_t, stat_options.px_size, seqLength);
if (stat_options.save_msd)
    msdPath = [fpath statpath 'msd'];
else
    msdPath = '';
end
[msd, motionType, fittedMSD, modelParams] = computeTrackMSD(tl, stat_options.delta_t, stat_options.px_size, seqLength, seqLength/5, msdPath);

%% export results

plotTracksEllipcesOverImage(tl, im, 2, stat_options.px_size, [], motionType, 2);
export_fig([fpath filesep statpath filesep fnameNE '_overlay_1frame_w_ids'], '-tiff', '-r400', '-a1');

printStatToTxt(stat, [], modelParams, [fpath statpath fnameNE '_stat_full.txt']);
save([fpath statpath fnameNE '_stat_full.mat'], 'stat', 'modelParams');
if ~isempty(stat_options.xls_path)
    xlsFname = [stat_options.xls_path filesep fnameNE '_stat.xlsx'];
else
    xlsFname = [fpath  filesep  fnameNE '_stat.xlsx'];
end

printStatToXls(stat, [], modelParams, [], xlsFname, pout, [fpath filesep statpath]);
DeleteEmptyExcelSheets(xlsFname)

end