function computeCellMotionDF(fname, output_path, registration_options)
% inputs:
% fname - filename to the sequence file, sequence segmentation filename should be [fname(1:end-4) '_body.tif']
% output_path - the path to output DF files
% registration_options - options for registration method
% 
% example of registration_options
% 
% registration_options.poissonRatio = 0.4;
% registration_options.youngMod = 1e4;
% registration_options.compliance = 0.5;
% registration_options.thickness = 2e-6;
% registration_options.createMesh = true;
% registration_options.resPathPrefix = '';
% registration_options.spotsSuffix = '';

% registration_options.pntsN = 100;
% registration_options.triHmax = 15;

% registration_options.dummy_scene_fname = '..\cell_image_registration\Registration_Dummy.scn';
% registration_options.sofa_exe_path = '..\sofa-bin\runSofa.exe';
% registration_options.tmp_path = 'd:\tmp\';
% 

[fpath, fnameNE, ~] = fileparts(fname);
fnameOut = fnameNE;

if ~isfield(registration_options, 'pntsN')
    registration_options.tmp_path = 'c:\tmp\';
    mkdir(registration_options.tmp_path);
end

registration_options.tmp_path = [registration_options.tmp_path filesep datestr(now,'yyyymmdd_HHMMSSFFF')];

if ~exist(registration_options.tmp_path, 'dir')
    mkdir(registration_options.tmp_path);
end

%% create SOFA script
idd = round(rand*1e6);
newDummyFname =  [registration_options.dummy_scene_fname(1:end-4) num2str(idd) '.scn'];
copyfile(registration_options.dummy_scene_fname, newDummyFname);
[fnameOutRes, fnameScene] = createSOFAscript(newDummyFname, ...
                                [registration_options.tmp_path filesep fnameOut],...
                                registration_options.poissonRatio, ...
                                registration_options.youngMod, ...
                                registration_options.compliance, ...
                                registration_options.thickness);
delete(newDummyFname);
%%
fpathMesh = [registration_options.tmp_path filesep fnameOut];
fpathMeshRes = [fnameOutRes];

cellm = readimTiff([fname(1:end-4) '_body.tif']);

if ~isfield(registration_options, 'pntsN')
    registration_options.pntsN = 100;
end
if ~isfield(registration_options, 'triHmax')
    registration_options.triHmax = 15;
end
seqLength = imsize(cellm, 3);

%% create mesh
if registration_options.createMesh
    % clear dirs
    if exist([fpathMesh '_contour'], 'dir')
        rmdir([fpathMesh '_contour'], 's')
    end
    if exist([fpathMesh '_points'], 'dir')
        rmdir([fpathMesh '_points'], 's')
    end
    mkdir([fpathMesh '_contour'])
    mkdir([fpathMesh '_points'])
    % create mesh
    createMeshes2D(cellm, fpathMesh, registration_options.pntsN, registration_options.triHmax)
    % create points
    fnamePts = [fname(1:end-4) '_' registration_options.spotsSuffix '.mat'];
    if strfind(fnameNE, 'RA')
        fnamePts = strrep(fnamePts, '_RA', '');
    end
    if exist(fnamePts, 'file')
        load(fnamePts);
        pnts = [spots{1}(:,1:2) zeros([size(spots{1},1) 1])];
    else
        pnts = imsize(slice_ex(cellm,0))/2;
    end
    writeVTK_pd([fpathMesh '_points' filesep 'pts.vtk'], pnts);
end
%% run SOFA
% clear results dir
if exist([fpathMeshRes '_registered'], 'dir')
    rmdir([fpathMeshRes '_registered'], 's')
end
mkdir([fpathMeshRes '_registered'])

% disp([registration_options.sofa_exe_path ' -g batch -n ' num2str(seqLength*20) ' ' fnameScene]);
system([registration_options.sofa_exe_path ' -g batch -n ' num2str(seqLength*20) ' ' fnameScene]);
%% read meshes, convert to ff

fpathRes = [fpath filesep registration_options.resPathPrefix filesep output_path];

convertMeshesToFF([fpathMeshRes '_registered'], fpathRes, fname, imsize(cellm));
% convertMeshesToFF_Kto1([fpathMeshRes '_registered'], fpathRes, fname, imsize(cellm));

%% clean up and save
rmdir(registration_options.tmp_path, 's');

save([fpathRes filesep 'registration_options.mat'], 'registration_options');
