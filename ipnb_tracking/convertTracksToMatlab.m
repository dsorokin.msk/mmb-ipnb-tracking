function convertTracksToMatlab(fname, postprocess_tracks_options, verbose, px_size)

if nargin < 2
    postprocess_tracks_options.enable = 1;
    postprocess_tracks_options.min_seq_length = 350;
end

if nargin < 3
    verbose = 0;
end

if nargin < 4
    px_size = 0.04;
end

[fpath, fnameNE, ~] = fileparts(fname);
fpath = [fpath filesep];

ovlIcs = readimTiff([fpath fnameNE '_ovlIcs.tif']);

[tracksArr] = ICYtracksRead([fpath filesep fnameNE '_tracks.xml']);
%% subtracks split|join
tl = create_tracks_fromTrackArray(postprocess_tracks_options.min_seq_length, tracksArr);

if (postprocess_tracks_options.enable)
    tlNew = semiAutoTrackSplit(ovlIcs, tl, 8);
    tlNew1 = semiAutoTrackJoin(ovlIcs, tlNew, 10, 0);
    tlNew2 = semiAutoTrackJoin(ovlIcs, tlNew1, 25, 0);
    tlNew3 = semiAutoTrackDelete(ovlIcs, tlNew2, 80);
else
    tlNew3 = tl;
end

if (verbose)
    ellongTr = 25;
    areaTr = 80000;
    figure;
    hold off; plot_all_tracks_ellipce(tlNew3, [], 1, 1:length(tlNew3), px_size, ellongTr, areaTr)
end
% axis equal

save([fpath fnameNE '_tlNew3.mat'], 'tlNew3');

end