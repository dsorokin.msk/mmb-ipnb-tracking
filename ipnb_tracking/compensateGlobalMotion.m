function compensateGlobalMotion(fname, registration_options)

%%
tic;
computeCellMotionDF(fname, 'SOFA', registration_options)
disp('==== Registration took ====')
toc
%%
tracks = load([fname(1:end-4) '_tlNew3.mat']);
tracks = tracks.tlNew3;

[fpath, ~, ~] = fileparts(fname);
resPath = 'SOFA';
df_path = [fpath filesep resPath filesep];

applyDFtoTracks(df_path, tracks, 0);