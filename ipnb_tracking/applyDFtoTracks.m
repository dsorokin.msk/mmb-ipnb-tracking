function [tracks_reg] = applyDFtoTracks(df_path, tracks, writeSpotsImgs)
% applies the deformation fields located in df_path to tracks and saves the
% results to df_path
% if writeSpotsImgs = 1 - writes images with spots

if (nargin < 3)
    writeSpotsImgs = 0;
end

if ~exist(df_path, 'dir')
    warning(['The directory with DF ' df_path ' does not exist! The tracks were not compensated!']);
    return;
end

spots = tracks2spots(tracks);

rigidMpath = [df_path filesep ls([df_path filesep '*_rigidM.mat'])];
if exist(rigidMpath, 'file') == 2
    data = load(rigidMpath);
    MovlCur2First = data.MovlCur2First;
    spotsRA = ff_align_spots_rigid(spots, MovlCur2First);
else
    spotsRA = spots;
end

bcwrdFlag = 0;
ffXpath = [df_path filesep ls([df_path filesep '*_ffXsh_bcw.ics'])];
if exist(ffXpath, 'file') == 2
    ffXics = readim(ffXpath);
    bcwrdFlag = bcwrdFlag+1;
end
ffYpath = [df_path filesep ls([df_path filesep '*_ffYsh_bcw.ics'])];
if exist(ffYpath, 'file') == 2
    ffYics = readim(ffYpath);
    bcwrdFlag = bcwrdFlag+1;
end

if (bcwrdFlag == 2) 
    spotsReg = remap_centroids_all_bckwrd(spotsRA, ffXics, ffYics, 1, 1);
else
    error('Check the deformation fields files')
end

tracks_reg = spots2tracks(spotsReg, tracks);

fnameNE = erase(ffYpath, '_ffYsh_bcw.ics');
[~, fnameNE, ~] = fileparts(fnameNE);
save([df_path filesep fnameNE '_tracks_reg.mat'], 'tracks_reg');

spotsSuffix = 'spots';
if (writeSpotsImgs)   
    [spotsRegIm, spotsOvl] = plotPointsIcs(spotsReg, imsize(ffXics));
    fprintf('Writing spots imgs.');
    writeimTiff(spotsRegIm, [df_path fnameNE '_' spotsSuffix '_tNRsh_pts.tif']);
    fprintf('.');
    writeimTiff(spotsOvl, [df_path fnameNE '_' spotsSuffix '_tNRsh_pts_ovl.tif']);
    fprintf('\n.doing forRA.\n');
    [~, spotsOvl] = plotPointsIcs(spotsRA, imsize(ffXics));
    fprintf('.');
    writeimTiff(spotsOvl, [df_path fnameNE '_' spotsSuffix '_tNRsh_pts_ovl_RA.tif']);
    fprintf('.done!\n');
end

end