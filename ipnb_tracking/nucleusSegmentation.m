function nucleusSegmentation(fname, options, verbose, save_parameters)
% nucleus body segmentation

if nargin < 3
    verbose = 0;
end

if nargin < 4
    save_parameters = 1;
end

% [ics, fname] = readimByPartFilename(fpath, 'stretch.tif');
ics = readimTiff(fname);

[cellm, cSizes] = segmentNucleus(ics, options);
cellm = dip_image(cellm, 'bin');

if (verbose)
    b = slice_op('berosion', cellm, 1);
    b = cellm-b;
    ovl = overlay(stretch(ics, 0, 99.7), b, [255 0 0]);
    dipshow(ovl)
    dipstep on
end

writeimTiff(cellm, [fname(1:end-4) '_body.tif']);

if (save_parameters)
    [fpath,~,~] = fileparts(fname);
    segm_pars_dir = 'segm_pars';
    mkdir([fpath filesep segm_pars_dir]);
    save([fpath filesep segm_pars_dir filesep 'segmOptions.mat'], 'options');
    copyfile([func2str(options.preprocessFunction) '.m'], [fpath filesep segm_pars_dir filesep 'preprocessSegm.m']);
    copyfile([func2str(options.postprocessFunction) '.m'], [fpath filesep segm_pars_dir filesep 'postprocessSegm.m']);
end

end
