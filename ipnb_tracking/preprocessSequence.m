function preprocessSequence(fname, stretch_val, test_mode)

if nargin < 2
    stretch_val = 99;
end

if nargin < 3
    test_mode = 0;
end

% [ics, fname] = readimByPartfname(fpath, '.tif');

ics = readimTiff(fname);

if (test_mode)
    stretch(slice_ex(ics,0),0, stretch_val)
else
    disp('Streching the intensity...');
    ics_str = slice_op('stretch', ics, 0, stretch_val);
    ics_str = dip_image(ics_str, 'uint8');
    disp('Done!');
    dipshow(ics_str)
    dipstep on
    writeimTiff(ics_str, [fname(1:end-4) '_stretch.tif']);
end

end