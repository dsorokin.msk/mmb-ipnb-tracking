fname = '..\data-example\3(30)-test\3(30).tif';


%% preprocessing the image sequence
%
stretch_val = 99;
test_mode = 0; % set test mode to 1 if you want to test the parameters on the first frame of the sequence

preprocessSequence(fname, stretch_val, test_mode);

%% nucelus segmentation
%

% options.method              = 'triangle';
options.method              = 'cellsize';
% options.method              = 'otsu';
% options.method              = 'watershed';
options.magnCoef            = 1.0;
options.cellSize            = 70000;
options.preprocessFunction  = @preprocessSegm;
options.postprocessFunction = @postprocessSegm;

options.contSmoothSigma     = 10;
options.contSmoothThreshold = 0.40;
options.contSmoothKeepEdges    = 0;
options.contSmoothEdgeStrength = 0.1;

options.watershedSigma     = 2;
options.watershedDilSize   = 20;
options.watershedEroSize   = 10;
options.watershedClosSize   = 0;

% or just load the pre-saved options
load('segmOptions.mat');

verbose = 1;
save_parameters = 1;

nucleusSegmentation([fname(1:end-4) '_stretch.tif'], options, verbose, save_parameters)

%% find iPNBs and nucleoli
% 
par = struct;
par_nucleoli = struct;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
par.startInd = 0;
% par.finishInd = 10;
par.finishInd = []; % if empty - uses the whole sequence
par.seqLength = par.finishInd - par.startInd + 1;
par.CropToMask = 0;

% % % % % % % Spot detection parameters % % % % % % % %
par.sigma = 2.0; % sigma for Hessian
par.sizeTr = [10 400]; % threshold for spot sizes
par.trMagn = 1.2; % threshold magnification coefficient
par.numOfLvl = 3; % number of levels in Hessian scale-space
par.LocalMax = 0;
% % % % % % % Nucleoli detection parameters % % % % % % % %
par_nucleoli.sigma = 6.0; % sigma for Hessian
par_nucleoli.sizeTr = [500 0]; % threshold for spot sizes
par_nucleoli.trMagn = 1.0; % threshold magnification coefficient
par_nucleoli.numOfLvl = 2; % number of levels in Hessian scale-space
par_nucleoli.tr = 100; % threshold for thresholding method
par_nucleoli.openR = 12; % radius of opening
par_nucleoli.contSmoothSigma     = 2;
par_nucleoli.contSmoothThreshold = 0.5;

verbose = 0;
save_overlays = 1;

findNuclearBodies([fname(1:end-4) '_stretch.tif'], par, par_nucleoli, verbose, save_overlays);

%% tracking
% here one should perform tracking with ICY (http://icy.bioimageanalysis.org/) or any other software and save 
% the results to [fname(1:end-4) '_stretch_tracks.xml']
% 
% ICY workflow example:
% 1. Import the results of the previous step (�*_spots_ICY.txt�) by using Plugins -> Spot Detection Import and Export -> Import.
% 2. Track using the Spot tracking plugin: choose spots imported on the previous step as Detection set, Estimate parameters, Run tracking.
% 3. Export the tracking results from Track Manager to �*_tracks.xml� by File -> Save As� .

% 

%% convert tracks to matlab format and postprocess tracks by joining close subtracks
% 
postprocess_tracks_options = struct;
postprocess_tracks_options.enable = 1;
postprocess_tracks_options.min_seq_length = 350;
verbose = 1;
px_size = 0.04;

convertTracksToMatlab([fname(1:end-4) '_stretch.tif'], postprocess_tracks_options, verbose, px_size);

%% Nuclei motion compensation and application of the obtained deformation fields to tracks
% 
registration_options.poissonRatio = 0.4;
registration_options.youngMod = 1e4;
registration_options.compliance = 0.5;
registration_options.thickness = 2e-6;
registration_options.createMesh = true;
registration_options.resPathPrefix = '';
registration_options.spotsSuffix = '';

registration_options.pntsN = 100;
registration_options.triHmax = 15;

registration_options.dummy_scene_fname = '..\\spt-lib\\release\\cell_image_registration\\Registration_Dummy.scn';
registration_options.sofa_exe_path = '..\\spt-lib\\sofa-bin\\runSofa.exe';
registration_options.tmp_path = 'd:\\tmp\\';

compensateGlobalMotion([fname(1:end-4) '_stretch.tif'], registration_options)

%% compute statistics

stat_options.xls_path = '..\data-example\3(30)-test\';
stat_options.px_size = 0.04;
stat_options.delta_t = 1.0;
stat_options.save_msd = 0;

analyzeTracksMotionType([fname(1:end-4) '_stretch.tif'], 'SOFA', stat_options);

